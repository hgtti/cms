Simple CMS
==========

Designed for ease of use and simplicity. This is a fast and powerful CMS that uses a combination of Markdown and Jade templates.

Optional Features
-----------------

* Our user management is built with git. Simply create a git repo inside of the pages directory. We will take dates and author names
from the commit log and use them inside of the page as author names.
* Widgets, we have several widgets that will be generated at compile time. The include recent edits, users on the site, and
who has edited a file. (This relies on the git managed pages)

Setup
-----

The setup of the CMS is simple, just use markdown in the pages directory. Things placed at the root of this directory will become
menu links. Content placed in folders at the root will have a page generated with a list of its contents. This is also true
of all subfolders. The site will be organized exactly the same as your file structure.

To change the templates, either edit the existing templates in the templates directory or make your own and change the
related settings in settings.json to point to the templates you would prefer to use.

To setup user management, make a directory called pages and do `git init` just inside that folder.

Any includes you would like to use must go in the includes folder. The includes must go under the appropriate folder.

Includes
--------
All files placed here will be included at the root of the site. It is strongly recommended to use
the folders js, css, and images. To reference these items from your articles and the settings, please
use absolute paths. Should the site be mounted under a folder please set that in the settings.

Settings.json
-------------
* Templates - Configure which templates to use for the various types of pages.
* Site Config - These are relevant options for filling in variables in the templates.
* outputDir - Where to put the static files
* Include Authors (bool) - Should we include a panel suggesting who the authors of the file are?
* Max Depth - How deep should the menu go. (Deeper files can still be reached using index pages.)
* Static resources - These resources will be available at the root of the site and are just copied from the listed directories. (Whole directories are copied)

Templates
---------

If you want to change the theme of your website, simply adjust your template to fit your needs. Jade is very easy to use
and change.

Future Ideas
------------

* Search feature (either integrated google or custom in house search function)
* Custom pages (Pages would be written in jade, useful for custom layouts for special requirements)
* Allow changes from the web interface (still uses git commits as the core)


