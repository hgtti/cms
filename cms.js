var jade = require('jade');
var fs = require('fs');
var fse = require('fs-extra');
var path = require('path');
var settings = require('./settings.json');
var md = require('markdown-it')({html: true, linkify: true});

var markdownFilter = /^.+\.md$/i;

var staticFiles = new RegExp('(' + settings['staticResources'].join('|') + ')');

/**
 * Generate a page of its direct children.
 */
function Index(name, folder){
	this.name = name;
	this.href = folder;
	this.children = [];
	this.output = settings['outputDir'] + folder + 'index.html';
}
Index.prototype.add = function(page){
	this.children.push(page);
}
Index.prototype.addAll = function(children){
	children.forEach(function(item){
		this.children.push(item);
	});
}
Index.prototype.render = function(menu){
	this.children.forEach(function(el){
		el.render(menu);
	});
	var name = this.name;
	var directory = this.generateMenu();
	var page = Index.template(Object.create(settings['siteConfig'], {
		menu: {
			value: menu,
			enumerable: true
		},
		name: {
			value: name
		},
		directory: {
			value: directory,
			enumerable: true
		}
	}));
	var output = this.output;
	fse.mkdirs(path.dirname(this.output), function(err){
		if (err) throw err;
		fs.writeFile(output, page, function(err){
			if (err) throw err;
		});
	});
}
Index.prototype.generateMenu = function(depth){
	var menu = {};
	menu['Index'] = this.href;
	for (var i = 0; i < this.children.length; ++i){
		if (this.children[i] instanceof Index){
			if (settings['pruneEmptyDirectories'] && this.children[i].children.length == 0){
				continue;
			} else {
				menu[this.children[i].name] = this.children[i].generateMenu();
			}
		} else {
			menu[this.children[i].name] = this.children[i].href;
		}
	}
	return menu;
}
Index.template = jade.compileFile('templates/' + settings.templates.list);


/**
 * Generate an article page.
 */
function Article(name, file){
	this.name = name;
	this.file = settings['inputDir'] + file;
	this.href = file.replace(/md/i, 'html');
	this.output = settings['outputDir'] + file.replace(/md/i, 'html');
}
Article.prototype.parse = function(callback){
	fs.readFile(this.file, "ascii", function(err, data){
		if (err) throw err;

		callback(md.render(data));

	});
}
Article.prototype.render = function(menu){
	var output = this.output;
	this.parse(function(data){
		var page = Article.template(Object.create(settings['siteConfig'], {
			//Override or add properties
			menu: {
				value: menu,
				enumerable: true
			},
			content: {
				value: data
			}
		}));
		fse.mkdirs(path.dirname(output), function(err){
			if (err) throw err;
			fs.writeFile(output, page, function(err){
				if (err) throw err;
			});
		});
	});
}
Article.template = jade.compileFile('templates/' + settings.templates.article);

/**
 * Generate the root page
 */
function Home(){
	this.children = [];
	this.name = 'Home';
	this.output = settings['outputDir'] + '/index.html';
	this.href = '/';
}
Home.prototype.add = function(child){
	this.children.push(child);
}
Home.prototype.addAll = function(children){
	children.forEach(function(item){
		this.children.push(item);
	});
}
Home.prototype.render = function(menu){
	this.children.forEach(function(el){
		el.render(menu);
	});
	var page = Home.template(Object.create(settings['siteConfig'], {
		//Override or add properties
		menu: {
			value: menu,
			enumerable: true
		}
	}));
	var output = this.output;
	fse.mkdirs(path.dirname(this.output), function(err){
		if (err) throw err;
		fs.writeFile(output, page, function(err){
			if (err) throw err;
		});
	});

}
Home.prototype.generateMenu = function(depth){
	var menu = {};
	menu[this.name] = this.href;
	for (var i = 0; i < this.children.length; ++i){
		if (this.children[i] instanceof Index){
			menu[this.children[i].name] = this.children[i].generateMenu();
		} else {
			menu[this.children[i].name] = this.children[i].href;
		}
	}
	return menu;
}
Home.template = jade.compileFile('templates/' + settings.templates.home);

/**
 * @param {string} folder - Where to look.
 * @param {Home} root - What to add the data to.
 * @param {string} prefix - Optional prefix for tracking recursive path.
 */
function getFiles(folder, root, prefix){

	var files = fs.readdirSync(folder);

	if (!prefix){ //For tracking it on the web.
		prefix = "/";
	}

	for (var i = 0; i < files.length; ++i){

		if (staticFiles.test(path.basename(files[i]))){
			fse.copy(folder + "/" + files[i], settings['outputDir'] + "/" + files[i], {clobber: true}, function(err){
				if (err){
					console.error("An error occured copying a static resource!", err);
				}
			});
			continue; //Don't include static resources in the menu.
		}

		if (/^\./.exec(path.basename(files[i]))){
			continue; //Skip hidden files and folders.
		}

		var stat = fs.statSync(folder + "/" + files[i]);

		//If its a file, make an article and add it.
		if (files[i].match(markdownFilter) || stat.isFile()){
			root.add(new Article(files[i].replace(/\.md$/i,''), prefix + files[i]));
		} else if (stat.isDirectory()){ //If its a directory make an index and recurse into it.
			root.add(getFiles(folder + "/" + files[i], new Index(files[i], prefix + files[i] + '/'), prefix + files[i] + "/"));
		}

	}

	return root;

}

var home = getFiles(settings['inputDir'], new Home());

var menu = home.generateMenu();

console.log("Menu:", JSON.stringify(menu, null, 2));
console.log("Nodes:", JSON.stringify(home, null, 2));
console.log("Rendering...");

home.render(menu);


